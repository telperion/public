// ==UserScript==
// @name        Facebook Ad Blocker
// @namespace   https://gitlab.com/telperion
// @description Hides inline and sidebar ads
// @updateURL   https://gitlab.com/telperion/public/raw/master/facebook-ad-blocker.user.js
// @match       *://*.facebook.com/*
// @require     https://code.jquery.com/jquery-3.2.1.min.js
// @version     0.0.2
// @grant       none
// ==/UserScript==

var byeBye = [
	'Suggested Offer',
	'Suggested Post',
	'Featured For You',
	'A Video You May Like',
	'More From Related Pages',
	'Suggested Page',
	'Recommended For You',
	'People You May Know',
	'Related',
	'Connect With Facebook',
	'Popular Live Video',
	'Sponsored',
	'A Page Story You May Like',
	'Page Stories You May Like',
	'Popular Across Facebook'
];

function killInline() {
	$tag = $('div[id^="hyperfeed_story_id"]');
	$($tag).each(function(){
		$this = $(this);
		byeBye.forEach(function(bye){
			if(!$($this).is(':hidden')){
				if($($this).text().indexOf(bye) !== -1){
					$($this).hide();
				}
			}
		});
	});
}

function killSidebar() {
	var hitList = [
		$('#pagelet_ego_pane'),
		$('.ego_section'),
		$('div[data-ownerid^="hyperfeed_story_id]')
	];
	hitList.forEach(function(victim){
		victim.hide();
	});
    var elemTrending = document.getElementById('pagelet_trending_tags_and_topics');
    elemTrending.parentNode.removeChild(elemTrending);
}

$(document).ready(function(){
      killSidebar(); // Unreliable here due to FB loading style
    $(document).on('scroll', function(){
        killSidebar(); // More reliable killing of sidebar
        killInline();
    });
});
